import os
from web3 import Web3, HTTPProvider
from web3 import eth
os.environ['WEB3_INFURA_PROJECT_ID'] = 'd2862cd5106a4070b5bd73d5f07f93cd'
w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/d2862cd5106a4070b5bd73d5f07f93cd'))
address_cont = '0x44e8692aD6dcA00B14b878f10Cf6F32B78cB48b3'
abi_cont = [{"inputs":[{"internalType":"string","name":"q","type":"string"},{"internalType":"string[]","name":"vars","type":"string[]"}],"name":"add_voting","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"},{"internalType":"uint256","name":"var_id","type":"uint256"}],"name":"get_amount_votes","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"get_amount_votings","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"}],"name":"get_enabled","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"}],"name":"get_owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"}],"name":"get_vote_question","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"}],"name":"get_vote_variants","outputs":[{"internalType":"string[]","name":"","type":"string[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"}],"name":"stop_voting","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"idx","type":"uint256"},{"internalType":"uint256","name":"var_id","type":"uint256"}],"name":"vote","outputs":[],"stateMutability":"nonpayable","type":"function"}]
contract = w3.eth.contract(address=address_cont, abi=abi_cont)


def get_amount_votings():
    try:
        return contract.functions.get_amount_votings().call()
    except:
        return -1


def get_amount_votes(voting_id, variant_id):
    try:
        return contract.functions.get_amount_votes(voting_id, variant_id).call()
    except:
        return -1


def get_voting_enabled(voting_id):
    try:
        return contract.functions.get_enabled(voting_id).call()
    except:
        return -1


def get_voting_question(voting_id):
    try:
        return contract.functions.get_vote_question(voting_id).call()
    except:
        return -1


def get_voting_variants(voting_id):
    try:
        return contract.functions.get_vote_variants(voting_id).call()
    except:
        return -1


def get_amount_votings():
    try:
        return contract.functions.get_amount_votings().call()
    except:
        return -1


def create_voting(account, closed_key, question, answers):
    try:
        w3.eth.account = account
        transaction_for_set = contract.functions.add_voting(question, answers).buildTransaction({
            'chainId': 3,
            'gas': 300000,
            'gasPrice': w3.eth.gasPrice,
            'nonce': w3.eth.getTransactionCount(w3.eth.account),
            'value': 0
        })
        # Необходим закрытый ключ для подписании транзакции :(
        closedKey = closed_key
        sign_txt = eth.Account.sign_transaction(transaction_for_set, closedKey)
        hash_txn = w3.eth.sendRawTransaction(sign_txt.rawTransaction)
        return hash_txn.hex()
    except:
        return -1


def stop_voting(account, closed_key, voting_id):
    try:
        w3.eth.account = account
        transaction_for_set = contract.functions.stop_voting(voting_id).buildTransaction({
            'chainId': 3,
            'gas': 300000,
            'gasPrice': w3.eth.gasPrice,
            'nonce': w3.eth.getTransactionCount(w3.eth.account),
            'value': 0
        })
        closedKey = closed_key
        sign_txt = eth.Account.sign_transaction(transaction_for_set, closedKey)
        hash_txn = w3.eth.sendRawTransaction(sign_txt.rawTransaction)
        hash_txn.hex()
    except:
        pass


def vote_voting(account, closed_key, voting_id, variant_id):
    try:
        w3.eth.account = account
        transaction_for_set = contract.functions.vote(voting_id, variant_id).buildTransaction({
            'chainId': 3,
            'gas': 300000,
            'gasPrice': w3.eth.gasPrice,
            'nonce': w3.eth.getTransactionCount(w3.eth.account),
            'value': 0
        })
        closedKey = closed_key
        sign_txt = eth.Account.sign_transaction(transaction_for_set, closedKey)
        hash_txn = w3.eth.sendRawTransaction(sign_txt.rawTransaction)
        hash_txn.hex()
    except:
        pass
