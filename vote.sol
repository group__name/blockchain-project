pragma solidity >=0.7.0;
pragma experimental ABIEncoderV2;
 
contract VoteDataBase
{
    string[] questions;
    string[][] variants;
    uint256[][] amount_votes;
    bool[] enabled;
    address[][] voted;
    address[] owners;
 
    function add_voting(string memory q, string[] memory vars) public returns(uint256)
    {
        require(vars.length <= 10);
        variants.push({});
        for (uint i = 0; i < vars.length; i++) variants[variants.length - 1].push(vars[i]);
        questions.push(q);
        enabled.push(true);
        owners.push(msg.sender);
        amount_votes.push({});
        voted.push({});
        for (uint256 i = 0; i < vars.length; i++) amount_votes[amount_votes.length - 1].push(0);
        return (questions.length - 1);
    }
 
    function get_vote_question(uint256 idx) public view returns(string memory)
    {
        require(idx < enabled.length);
        return questions[idx];
    }
 
    function get_vote_variants(uint256 idx) public view returns(string[] memory)
    {
        require(idx < enabled.length);
        return variants[idx];
    }
 
    function get_amount_votes(uint256 idx, uint256 var_id) public view returns(uint256)
    {
        require(idx < enabled.length);
        return amount_votes[idx][var_id];
    }
 
    function get_owner(uint256 idx) public view returns(address)
    {
        require(idx < enabled.length);
        return owners[idx];
    }
 
    function get_enabled(uint256 idx) public view returns(bool)
    {
        require(idx < enabled.length);
        return enabled[idx];
    }
 
    function stop_voting(uint256 idx) public
    {
        require(idx < enabled.length);
        require(msg.sender == owners[idx]);
        enabled[idx] = false;
    }
 
    function get_amount_votings() public view returns(uint256)
    {
        return enabled.length;
    }
 
    function vote(uint256 idx, uint256 var_id) public
    {
        require(idx < enabled.length);
        require(enabled[idx] == true);
        bool ok = true;
        for (uint i = 0; i < voted[idx].length; i++) if (msg.sender == voted[idx][i]) ok = false;
        require(ok == true);
        voted[idx].push(msg.sender);
        amount_votes[idx][var_id] += 1;
    }
}
//https://ropsten.etherscan.io/address/0x44e8692ad6dca00b14b878f10cf6f32b78cb48b3
